from django.urls import path
from .views import *
app_name = 'products'

urlpatterns = [
    path('/createSKU', createProduct, name="create"),
    path('', productActions, name="productActions"),
]
