from .models import *
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
from django.forms.models import model_to_dict


def checkValidCategory(category):
    error = ""
    isValid = True
    if category not in dict(PRODUCT_CATEGORY).keys():
        error = "Invalid Category {category}".format(category=category)
        isValid = False
    return isValid, error


@require_http_methods(["POST"])
def createProduct(request):
    if request.method == 'POST':
        responseBody = {}
        requestBody = json.loads(request.body)
        isValid, error = checkValidCategory(requestBody['category'])
        if not isValid:
            responseBody['status_code'] = 400
            responseBody['message'] = error
            return JsonResponse(responseBody)
        try:
            Product.objects.create(
                sku_name=str(requestBody['name']), sku_price=int(requestBody['price']), sku_category=requestBody['category']
            )
            responseBody['status_code'] = 200
            responseBody['message'] = "Product created successfully"
        except ValueError:
            responseBody['status_code'] = 400
            responseBody['message'] = "Price must be integer"
        except Exception as e:
            print(e)
            responseBody['status_code'] = 500
            responseBody['message'] = "Internal Server Error"
        return JsonResponse(responseBody)


@require_http_methods(["GET", "PUT", "DELETE"])
def productActions(request):
    responseBody = {}
    try:
        product = Product.objects.get(pk=request.GET.get('id'))

        # Get details
        if request.method == "GET":
            responseBody['status_code'] = 200
            responseBody['message'] = model_to_dict(product)

        # Update details
        elif request.method == "PUT":
            requestBody = json.loads(request.body)
            if requestBody.get('category'):
                isValid, error = checkValidCategory(requestBody['category'])
                if not isValid:
                    responseBody['status_code'] = 400
                    responseBody['message'] = error
                    return JsonResponse(responseBody)

            product.sku_name = product.sku_name if not requestBody.get(
                "name") else str(requestBody.get("name"))
            product.sku_category = product.sku_category if not requestBody.get(
                "category") else requestBody.get("category")

            product.sku_price = product.sku_price if not requestBody.get(
                "price") else int(requestBody.get("price"))

            product.save()
            responseBody['status_code'] = 200
            responseBody['message'] = "Product id {id} updated successfully".format(
                id=product.id)

        # Delete product
        elif request.method == "DELETE":
            product = product.delete()
            responseBody['status_code'] = 200
            responseBody['message'] = "Product id {id} deleted successfully".format(
                id=request.GET.get('id'))

        # Other methods
        else:
            responseBody['status_code'] = 404
            responseBody['message'] = "Page not found"

    except Product.DoesNotExist:
        responseBody['status_code'] = 400
        responseBody['message'] = "Product does not exist"
    except ValueError:
        responseBody['status_code'] = 400
        responseBody['message'] = "Price must be integer"
    except Exception as e:
        print(e)
        responseBody['status_code'] = 500
        responseBody['message'] = "Internal Server Error"

    return JsonResponse(responseBody)
